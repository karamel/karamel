FROM python:alpine
MAINTAINER dev@easymov.fr

ADD . /program
WORKDIR /program

RUN pip install .
