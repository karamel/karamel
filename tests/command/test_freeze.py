import unittest, sys
import os, tempfile, shutil

from karamel.command.freeze import *
from karamel.command.install import *
from karamel.exception import KaramelException

class TestFreezeCommand(unittest.TestCase):

    def setUp(self):
        self.tmp = tempfile.mkdtemp()
        self.current = os.getcwd()
        os.chdir(self.tmp)

        packages_dir = os.path.join(self.tmp, 'packages')
        packages_manager = ['https://gitlab.com/karamel/tests/package_manager/raw/master/packages.yml']
        self.freeze = FreezeCommand(packages_manager, packages_dir)
        self.install = InstallCommand(packages_manager, packages_dir)

        freeze_file_name = 'freeze.txt'
        freeze_file = \
'''package_1==0.1.0
package_2==master'''
        with open(freeze_file_name, 'w') as stream:
            stream.write(freeze_file)
        sys.argv = ['', '-f', 'freeze.txt']
        self.install.run()

    def tearDown(self):
        os.chdir(self.current)
        shutil.rmtree(self.tmp)

    def test_command_run_freeze_all(self):
        sys.argv = ['']
        self.freeze.run()

    def test_command_run_freeze_one(self):
        sys.argv = ['', 'package_1']
        self.freeze.run()

    def test_command_run_freeze_several(self):
        sys.argv = ['', 'package_1', 'package_2']
        self.freeze.run()

    def test_command_run_freeze_invalid(self):
        sys.argv = ['', 'package_dejdiejdie']
        self.freeze.run()
