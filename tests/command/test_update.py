import unittest, sys
import os, tempfile, shutil

from karamel.command.install import *
from karamel.command.update import *
from karamel.exception import KaramelException

class TestUpdateCommand(unittest.TestCase):

    def setUp(self):
        self.tmp = tempfile.mkdtemp()
        self.current = os.getcwd()
        os.chdir(self.tmp)

        self.packages_dir = os.path.join(self.tmp, 'packages')
        packages_manager = ['https://gitlab.com/karamel/tests/package_manager/raw/master/packages.yml']
        self.install = InstallCommand(packages_manager, self.packages_dir)
        self.update = UpdateCommand(packages_manager, self.packages_dir)

    def tearDown(self):
        os.chdir(self.current)
        shutil.rmtree(self.tmp)

    def test_command_run(self):
        sys.argv = ['', 'package_1']
        self.install.run()
        self.update.run()

    def test_command_run_not_installed(self):
        sys.argv = ['', 'package_1']
        with self.assertRaises(KaramelException):
            self.update.run()

    def test_command_run_not_found(self):
        sys.argv = ['', 'package_fkdoekodeko']
        with self.assertRaises(KaramelException):
            self.update.run()
