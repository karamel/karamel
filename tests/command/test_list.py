import unittest, sys
import os, tempfile, shutil

from karamel.command.install import *
from karamel.command.list import *
from karamel.exception import KaramelException

class TestUninstallCommand(unittest.TestCase):

    def setUp(self):
        self.tmp = tempfile.mkdtemp()
        self.current = os.getcwd()
        os.chdir(self.tmp)

        self.packages_dir = os.path.join(self.tmp, 'packages')
        packages_manager = ['https://gitlab.com/karamel/tests/package_manager/raw/master/packages.yml']
        self.install = InstallCommand(packages_manager, self.packages_dir)
        self.list = ListCommand(packages_manager, self.packages_dir)

    def tearDown(self):
        os.chdir(self.current)
        shutil.rmtree(self.tmp)

    def test_command_run(self):
        sys.argv = ['']
        self.list.run()

    def test_command_run_with_arg_installed(self):
        sys.argv = ['', 'package_1']
        self.install.run()
        sys.argv = ['', '--installed']
        self.list.run()
