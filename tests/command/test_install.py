import unittest, sys
import os, tempfile, shutil

from karamel.command.install import *
from karamel.exception import KaramelException

class TestInstallCommand(unittest.TestCase):

    def setUp(self):
        self.tmp = tempfile.mkdtemp()
        self.current = os.getcwd()
        os.chdir(self.tmp)

        packages_dir = os.path.join(self.tmp, 'packages')
        packages_manager = ['https://gitlab.com/karamel/tests/package_manager/raw/master/packages.yml']
        self.command = InstallCommand(packages_manager, packages_dir)

    def tearDown(self):
        os.chdir(self.current)
        shutil.rmtree(self.tmp)

    def test_command_run(self):
        sys.argv = ['', 'package_1']
        self.command.run()

    def test_command_run_package_not_found(self):
        sys.argv = ['', 'package_not_found']
        self.command.run()

    def test_command_run_package_already_installed(self):
        sys.argv = ['', 'package_1']
        self.command.run()

    def test_command_run_package_with_version(self):
        sys.argv = ['', 'package_1==0.1.0']
        self.command.run()

    def test_command_run_package_with_invalid_version(self):
        sys.argv = ['', 'package_1==invalid']
        self.command.run()

    def test_command_run_package_reinstalled_with_different_version(self):
        sys.argv = ['', 'package_1==0.1.0']
        self.command.run()
        sys.argv = ['', 'package_1==0.2.0']
        self.command.run()

    def test_command_run_package_reinstalled_with_same_version(self):
        sys.argv = ['', 'package_1==0.1.0']
        self.command.run()
        sys.argv = ['', 'package_1==0.1.0']
        self.command.run()

    def test_command_run_install_with_freeze(self):
        freeze_file_name = 'freeze.txt'
        freeze_file = \
'''package_1==0.1.0
package_2==master'''
        with open(freeze_file_name, 'w') as stream:
            stream.write(freeze_file)

        sys.argv = ['', '-f', 'freeze.txt']
        self.command.run()
