import unittest, sys
import os, tempfile, shutil

from karamel.command.command import *

class TestCommand(unittest.TestCase):

    def setUp(self):
        self.command = Command('command', 'description')

    def test_command_run(self):
        sys.argv = []
        self.command.run()

    def test_command_run_with_argument(self):
        sys.argv = ['', 'value']
        self.command.add_argument('value')
        self.command.run()

    def test_command_run_missing_argument(self):
        sys.argv = ['']
        self.command.add_argument('value')
        with self.assertRaises(SystemExit):
            self.command.run()

    def test_command_run_with_command(self):
        sys.argv = ['', 'sub_command']
        sub_command = Command('sub_command', 'description')
        self.command.add_command(sub_command)
        self.command.run()
